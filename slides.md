% Evolutionary Algorithms
% Tom Eccles

## Introduction

I aim to give a lightning fast introduction - leaving lots out.

- What is Evolution?
- Optimisation Algorithms
- Fitness landscapes
- Coevolution
- Video

## Terminology

There are a number of schools with confusing names but specific meanings:

- Genetic Algorithms
	- Sexual reproduction
- Evolutionary Strategies
	- Asexual reproduction
- Genetic Programming
	- LISP S-Expressions
- Evolutionary Programming
	- Finite State Machines

I am not talking about any of these in particular so I will try to say
"Evolutionary Algorithms"

# What is Evolution?

## A Definition
"The change in (the genetic make-up of) a population e.g. by Natural Selection"
- Dr Richard Watson (2018)

## Natural Selection
- Reproduction
- Variation
- Heredity
- Variations cause differences in fitness, which apply selection pressure.

In Biology fitness is roughly long term reproductive success *for that gene*.
When using evolutionary algorithms to solve engineering problems we can evaluate
fitness however we like.

## Example 1: Biological Life
- Reproduction
	- Cell division
- Variation
	- Random mutation and sexual recombination
- Heredity
	- DNA is passed between generations
- Variations cause differences in fitness
	- Sometimes. It is really complicated.

## Example 2: Memes
The term "meme" was first coined by Richard Dawkins writing about Evolution in "The Selfish Gene" (1976).

"[A meme is] a unit of cultural transmission, or a unit of
imitation"

- Reproduction
	- People generally re-tell stories and ideas - it is much easier than
	re-inventing the wheel
- Variation
- Heredity
	- When re-telling stories, especially in oral cultures, the important parts
	are kept the same, while details might change. This is how memory works.
	For example, the "Hero's Journey" story goes all of the way back to one of the
	first written stories (The Epic of Gilgamesh) but is changed to fit the
	audience (Enkidu was created by the Gods of Mesopotamia, Spiderman was created
	by a radioactive spider).
- Variations cause differences in fitness
	- If you tell bad stories then nobody will listen or remember them.

## Example 3: Crystals

- Reproduction?
- Variation?
- Heredity?
- Variations cause differences in fitness?

# Optimisation Algorithms

## Stochastic Hill-climber
- Start with a random solution
- Repeat
	- Make a random change
	- If it is an improvement, keep it; else discard it and keep the original

![](graph_hill_climb.png){width=50%}

## A different Hill-climber
- Make two random individuals
- Repeat
	- Make a variant of the better individual, replacing the worse individual

![](graph_hill_climb.png){width=50%}

## One step further
- Make P individuals
- Repeat
	- Make variations of the better individuals, discard the worse individuals

![](graph_population.png){width=50%}

## Why not use a normal hill climber?
- Biology seems to have worked well
- There is the possibility of crossover to generate new solutions from multiple
  parents
- Evolution with only asexual reproduction is roughly a parallel (stochastic)
	hill climber.

## Components of an Evolutionary Algorithm
- Representation of individuals
	- Generally tailored to the problem (e.g. a game playing AI, S-Expressions, etc)
- A function to evaluate fitness
	- f(individual) = positivie real number
- A selection mechanism
	- Pick individuals proportional to fitness
	- Tournament
- Variation operators
	- Mutation (asexual)
	- Crossover or recombination (sexual)

# Fitness Landscapes

## What is a fitness landscape?
- Fitness plotted over varying genes
- Typically very highly dimensional

[](graph_raw.jpg){width=50%}

## Biological example
- This is in "phenotype space" not genes

![](biological-landscape1.png){width=25%}
![](biological-landscape2.png){width=50%}

## What problems can we have?
- All the same as any optimisation algorithm:
- Ruggedness
- Local optima
- Zero gradient (many gene variations make little or no difference)

## Darwin
- "If it could be demonstrated that any complex organ existed which could not
  possibly have been formed by numerous, successive, slight modifications, my
  theory would absolutely break down" - Charles Darwin, 1859

![](graph_jump.png){width=50%}

## How do we get between peaks?
- If mutations are distributed uniformly, the expected time to solve a local
  optima is exponential in the number of genes which need to be gotten right before
  there is a supporting fitness gradient.
- The central problem of any search problem is to split these jumps into modules
  which can be solved independently: exp(n/2) + exp(n/2) << exp(n/2) * exp(n/2) = exp(n)
- This may be the point of having multiple parents: to combine different modules

## Engineering a landscape
- Small changes to genotypes define their neighbourhood and so the
  shape of the fitness landscape
- Finding the right gene encoding and mutatation operator(s) can turn an exponential
  peak-jump into a linear convex optimisation.

# Coevolution

## What if there is no absolute fitness?
- In many problems it is difficult to construct an absolute measure of fitness
- Instead fitness is subjective: f(i1, i2) = x
- For example,
	- Chess
	- Predator/prey
- Arms races between populations can provide a good fitness gradient

## Problems
- Convergence
	- We may never settle on a solution
- Loss of gradient
	- E.g. all of population 1 beats all of population 2
- Overspecialisation
- Intransitivity
	- Rock paper scissors: Rock > Scissors, Scissors > Paper, Paper > Rock
- "Collusion" in mediocre stable states with no fitness gradient

# Video

## Evolved virtual creatures by Karl Sims (1994)
- [Link](http://www.karlsims.com/evolved-virtual-creatures.html)
- What is happening at 01:04?

## Final words
- As shown in the video, it is high fitness which is learnt, not always what you
  had in your head.

# Any Questions?

## Credit
This presentation was based upon COMP6202 Evolution of Complexity taught by Dr Richard Watson of Southampton University in 2018/19.
