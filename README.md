# Evolution Presentation

Presentation about evolution algorithms.

Based upon COMP6202 Evolution of Complexity as taught by Dr Richard Watson of Southampton University in 2018/19.

See CI build artefacts for PDF.
